import AppConstants from '../constants/app-constants';
import { dispatch, register} from '../dispatchers/app-dispatcher';

export default {
  fetchUsers(){
    dispatch({type: 'FETCH_USERS'});
  },
  fetchUser(userId){
    dispatch({type: 'FETCH_USER', userId});
  },
  fetchProjects(){
    dispatch({type: 'FETCH_PROJECTS'});
  },
  fetchTeams(){
    dispatch({type: 'FETCH_TEAMS'});
  },
  updateUser(userData){
    dispatch({type: 'UPDATE_USER', userData});
  }
}
