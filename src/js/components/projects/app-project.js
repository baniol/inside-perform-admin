import React from 'react';
import {Link} from 'react-router';

export default (props) => {
  return (
    <div className="item">
      <Link className="header" to={`/project/${props.project.id}`}>{props.index}) {props.project.projectName}</Link>
      <div className="description">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{props.project.productName}</div>
    </div>
  );
};