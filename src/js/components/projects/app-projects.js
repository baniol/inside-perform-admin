import React from 'react';
import AppActions from '../../actions/app-actions';
import AppStore from '../../stores/app-store';
import Project from './app-project';

export default class extends React.Component {
  constructor() {
    super();
    this.state = {projects: AppStore.getProjects()};
    this._onChange = this._onChange.bind(this);
  }

  componentWillMount() {
    AppStore.addChangeListener(this._onChange)
    AppActions.fetchProjects();
  }

  componentWillUnmount() {
    AppStore.removeChangeListener(this._onChange)
  }

  _onChange() {
    var projects = AppStore.getProjects();
    this.setState({projects: projects})
  }

  render() {
    var projects = this.state.projects.map((project, index) => {
      index++;
      return <Project key={project.id} index={index} project={project}/>;
    });
    return (
      <div className="ui relaxed divided list">
        {projects}
      </div>
    );
  }
};

