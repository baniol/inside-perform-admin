import React from 'react';
import AppActions from '../../actions/app-actions';
import AppStore from '../../stores/app-store';
import User from './app-user';

export default class extends React.Component {
  constructor() {
    super();
    this.state = {users: AppStore.getUsers()};
    // @TODO what is the purpose of this?
    this._onChange = this._onChange.bind(this);
  }

  componentWillMount() {
    AppStore.addChangeListener(this._onChange)
    AppActions.fetchUsers();
  }

  componentWillUnmount() {
    AppStore.removeChangeListener(this._onChange)
  }

  _onChange() {
    var users = AppStore.getUsers();
    this.setState({users: users})
  }

  render() {
    var users = this.state.users.map((user, index) => {
      index++;
      return <User key={user.id} index={index} user={user}/>;
    });
    return (
      <div className="ui relaxed divided list">
        {users}
      </div>
    );
  }
};

