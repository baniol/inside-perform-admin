import React from 'react';
import AppActions from '../../actions/app-actions';
import AppStore from '../../stores/app-store';
import update from 'react-addons-update'

export default class extends React.Component {
  constructor() {
    super();
    this.state = {
      user: {},
      projects: [],
      teams: [],
      formStatus: '',
      formMessageStatus: 'success',
      messageHeader: '',
      messageContent: '',
      formErrors: [],
      fieldErrors: {
        deskId: '',
        birthday: ''
      }
    };
    // https://facebook.github.io/react/blog/2015/01/27/react-v0.13.0-beta-1.html#autobinding
    this._onChange = this._onChange.bind(this);
    this.changeUserProp = this.changeUserProp.bind(this);
  }

  componentWillMount() {
    AppStore.addChangeListener(this._onChange);
    AppActions.fetchUser(this.props.params.user);
    AppActions.fetchProjects();
    AppActions.fetchTeams();
  }

  initSelect() {
    $('.ui.dropdown')
      .dropdown({
        maxSelections: 3
      })
  }

  componentWillUnmount() {
    AppStore.removeChangeListener(this._onChange);
  }

  _onChange() {
    var user = AppStore.getUser();
    var projects = AppStore.getProjects();
    var teams = AppStore.getTeams();
    this.setState({projects: projects, teams: teams});
    this.setState({formStatus: AppStore.getFormStatus()});
    this.setState({formMessageStatus: AppStore.getFormMessageStatus()});
    this.setState({messageHeader: AppStore.getMessageHeader()});
    this.setState({messageContent: AppStore.getMessageContent()});
    this.setState({formErrors: AppStore.getFormErrors()});
    if (this.state.formErrors.length > 0) {
      this.handleErrors();
    }
    else {
      this.setState({user: user});
    }
    //if (projects.length > 0) {
    //  this.initSelect();
    //}
  }

  handleErrors() {
    var errors = this.state.formErrors;
    errors.forEach(el => {
      this.state.fieldErrors[el.param] = 'error';
    });
  }

  changeUserProp(e) {
    var element = e.target;
    var newState = update(this.state, {user: {name: {$set: element.value}}});
    switch (element.name) {
      case 'name':
        newState = update(this.state, {user: {name: {$set: element.value}}});
        break;
      case 'position':
        newState = update(this.state, {user: {position: {$set: element.value}}});
        break;
      case 'email':
        newState = update(this.state, {user: {email: {$set: element.value}}});
        break;
      case 'deskid':
        newState = update(this.state, {user: {deskId: {$set: element.value}}});
        break;
      case 'project':
        var projectId = parseInt(element.options[element.selectedIndex].value);
        newState = update(this.state, {user: {projectId: {$set: projectId}}});
        break;
      case 'team':
        var teamId = parseInt(element.options[element.selectedIndex].value);
        newState = update(this.state, {user: {teamId: {$set: teamId}}});
        break;
      case 'birthday':
        newState = update(this.state, {user: {birthday: {$set: element.value}}});
        break;
    }
    this.setState(newState);
  }

  onSubmit(e) {
    e.preventDefault();
    AppActions.updateUser(this.state.user);
  }

  render() {
    var date = this.state.user && this.state.user.birthday ? this.state.user.birthday.substr(0, 10) : '0000-00-00';

    var projectOptions = this.state.projects.map((project) => {
      return (<option key={project.id} value={project.id}>{project.projectName}</option>);
    });
    var teamOptions = this.state.teams.map((team) => {
      return (<option key={team.id} value={team.id}>{team.name}</option>);
    });
    var src = this.state.user.image || 'http://placehold.it/150x150';

    return (
      <div className="ui grid">
        <div className="ten wide column">
          <form className={"ui form " + this.state.formStatus} onSubmit={this.onSubmit.bind(this)}>

            <div className={"ui message " + this.state.formMessageStatus}>
              <div className="header">{this.state.messageHeader}</div>
              <p>{this.state.messageContent}</p>
            </div>

            <div className="field">
              <label>Name</label>
              <input type="text" name="name" placeholder="Name" value={this.state.user.name}
                     onChange={this.changeUserProp}/>
            </div>
            <div className="field">
              <label>Position</label>
              <input type="text" name="position" placeholder="Position" value={this.state.user.position}
                     onChange={this.changeUserProp}/>
            </div>
            <div className="field">
              <label>Email</label>
              <input type="email" name="email" placeholder="Email" value={this.state.user.email}
                     onChange={this.changeUserProp}/>
            </div>
            <div className={"field " + this.state.fieldErrors.deskId}>
              <label>Desk ID</label>
              <input type="number" name="deskid" placeholder="Desk ID" value={this.state.user.deskId}
                     onChange={this.changeUserProp}/>
            </div>
            <div className="field">
              <label>Project</label>
              <select className="ui fluid dropdown" name="project" value={this.state.user.projectId}
                      onChange={this.changeUserProp}>
                {projectOptions}
              </select>
            </div>
            <div className="field">
              <label>Team</label>
              <select className="ui fluid dropdown" name="team" value={this.state.user.teamId}
                      onChange={this.changeUserProp}>
                {teamOptions}
              </select>
            </div>
            <div className={"field " + this.state.fieldErrors.birthday}>
              <label>Birthday</label>
              <input type="text" name="birthday" className="birth-date" placeholder="Birthday" value={date}
                     onChange={this.changeUserProp}/>
            </div>
            <button className="ui button" type="submit">Submit</button>
          </form>
        </div>
        <div className="six wide column">
          <img src={src}/>
        </div>
      </div>

    );
  }
};

