import React from 'react';
import {Link} from 'react-router';

export default (props) => {
  return (
    <div className="item">
      <Link className="header" to={`/user/${props.user.id}`}>{props.index}) {props.user.name}</Link>
      <div className="description">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{props.user.productName}</div>
    </div>
  );
};