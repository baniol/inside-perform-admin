import React from 'react';
import Dashboard from './dashboard/app-dashboard';
import Users from './users/app-users';
import EditUser from './users/app-edituser';
import Projects from './projects/app-projects';
import Template from './app-template';
import { Router, Route, IndexRoute } from 'react-router';

// @TODO : change to <Router>{routes}</Router>

export default () => {
  return (
    <Router>
      <Route path="/" component={ Template }>
        <IndexRoute component={ Dashboard }/>
        <Route path="users" component={ Users }/>
        <Route path="user/:user" component={ EditUser }/>
        <Route path="projects" component={ Projects }/>
      </Route>
    </Router>
  );
};
