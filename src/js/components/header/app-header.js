import React from 'react';
import { Link } from 'react-router';

export default () => {
  return (
    <div className="ui fixed inverted menu">
      <div className="ui container">
        <a href="#" className="header item">
          Inside Perform
        </a>
        <a href="#" className="item">Home</a>
        <Link to={ `/users` } className="item">Users</Link>
        <Link to={ `/projects` } className="item">Projects</Link>
      </div>
    </div>
  );
};
