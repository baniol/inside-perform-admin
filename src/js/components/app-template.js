import React from 'react';
import Header from './header/app-header';

export default (props) => {
  return (
    <div className="container">
      <Header />
      <div className="ui main text container">
        {/*<h1 className="ui header">Semantic UI Fixed Template</h1>*/}
        { props.children }
      </div>
    </div>
  );
};

