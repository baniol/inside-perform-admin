import request from 'superagent';

const URL = 'http://192.168.33.13:3000/api/';

const API = {
  fetchUsers(cb) {
    request.get(`${URL}getPeople`)
      .end((err, res) => {
        cb(err, res.body);
      });
  },
  fetchUser(userId, cb) {
    request.get(`${URL}getPerson/${userId}`)
      .end((err, res) => {
        cb(err, res.body);
      });
  },
  fetchProjects(cb) {
    request.get(`${URL}getProjects`)
      .end((err, res) => {
        cb(err, res.body);
      });
  },
  fetchTeams(cb) {
    request.get(`${URL}getTeams`)
      .end((err, res) => {
        cb(err, res.body);
      });
  },
  updateUser(userData, cb) {
    request.post(`${URL}updatePerson/${userData.id}`)
      .send(userData)
      .set('Accept', 'application/json')
      .end(function (err, res) {
        cb(err, res);
      });
  }
};

export default API;