import {dispatch, register} from '../dispatchers/app-dispatcher';
import AppConstants from '../constants/app-constants';
import { EventEmitter } from 'events';
import userAPI from '../api/userAPI';

const CHANGE_EVENT = 'change';

var users = [];
var user;
var projects = [];
var teams = [];
var formStatus = '';
var formMessageStatus = 'success';
var messageHeader = '';
var messageContent = '';
var formErrors = [];

const AppStore = Object.assign(EventEmitter.prototype, {
  emitChange(){
    this.emit(CHANGE_EVENT)
  },

  addChangeListener(callback){
    this.on(CHANGE_EVENT, callback)
  },

  removeChangeListener(callback){
    this.removeListener(CHANGE_EVENT, callback)
  },

  getUsers() {
    return users;
  },

  getUser() {
    return user;
  },

  getProjects() {
    return projects;
  },

  getTeams() {
    return teams;
  },

  getFormStatus() {
    return formStatus;
  },

  getFormMessageStatus() {
    return formMessageStatus;
  },

  getMessageHeader(){
    return messageHeader;
  },

  getMessageContent(){
    return messageContent;
  },

  getFormErrors() {
    return formErrors;
  },

  fetchUsers(res) {
    users = res;
    AppStore.emitChange();
  },

  fetchUser(res) {
    user = res[0];
    AppStore.emitChange();
  },

  fetchTeams(res) {
    teams = res;
    AppStore.emitChange();
  },

  fetchProjects(res) {
    projects = res;
    AppStore.emitChange();
  },

  dispatcherIndex: register((action) => {
    switch (action.type) {
      case AppConstants.FETCH_USERS:
        userAPI.fetchUsers((err, res) => {
          // @TODO handle error
          AppStore.fetchUsers(res);
        });
        break;
      case AppConstants.FETCH_USER:
        userAPI.fetchUser(action.userId, (err, res) => {
          // @TODO handle error
          AppStore.fetchUser(res);
        });
        break;
      case AppConstants.FETCH_PROJECTS:
        userAPI.fetchProjects((err, res) => {
          // @TODO handle error
          AppStore.fetchProjects(res);
        });
        break;
      case AppConstants.FETCH_TEAMS:
        userAPI.fetchTeams((err, res) => {
          // @TODO handle error
          AppStore.fetchTeams(res);
        });
        break;
      case AppConstants.UPDATE_USER:
        userAPI.updateUser(action.userData, (err, res) => {
          // @TODO handle error
          if(err) {
            var errors = JSON.parse(res.text);
            formStatus = 'error';
            formMessageStatus = 'error';
            //console.log(errors);
            formErrors = errors;
            messageHeader = 'There are errors in the submitted form';
            messageContent = 'Correct the errors and resend the form';
          }
          else {
            formStatus = 'success';
            formMessageStatus = 'success';
            messageHeader = 'User data saved!'
          }
          AppStore.emitChange();
        });
        break;
    }

  })
});

export default AppStore;
